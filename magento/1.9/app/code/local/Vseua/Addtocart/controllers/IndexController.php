<?php
class Vseua_Addtocart_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
        $h = $this->getRequest()->getParam('h');
        if($h !== null) {
          $cart = base64_decode($h);
          $jsonData = json_decode($cart);
          if (!empty($jsonData)) {
            $cart = Mage::getModel('checkout/cart');
            $cart->init();
            /* @var $pModel Mage_Catalog_Model_Product */
            foreach ($jsonData as $key => $productData) {
                $productId = $productData->id;
                $amount = $productData->amount;
                foreach ($cart->getQuote()->getAllItems() as $item) {
                  if ($productId == $item->getProductId() ) {
                    $cart->getQuote()->deleteItem($item);
                  }
                }
                $pModel = Mage::getModel('catalog/product')->load($productId);
                if ($pModel->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                    try {
                        $cart->addProduct($pModel, ['qty' => $amount]);
                    }
                    catch (Exception $e) {
                        continue;
                    }
                }
            }
            $cart->save();
          }
        }
        $this->_redirect('checkout/cart');
    }
}