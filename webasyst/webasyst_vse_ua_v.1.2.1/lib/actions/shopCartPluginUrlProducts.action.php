<?php

class shopCartPluginUrlProductsAction extends shopFrontendAction
{
    public function execute()
    {
        $hParam = $_POST['h'];
        $cart = base64_decode($hParam);
        $jsonData = json_decode($cart);

        if(!empty($jsonData)) {
            foreach ($jsonData as $data) {
                $id = $data->id;
                $amount = $data->amount;

                $_POST = [];
                $_POST['id'] = $id;
                waRequest::setParam('noredirect', true);
                $data = waRequest::post();
                $this->deteleToCart($data);

                $_POST = [];
                $_POST['quantity'] = $amount;
                $_POST['id'] = $id;
                waRequest::setParam('noredirect', true);
                $data = waRequest::post();
                $this->addToCart($data);
            }
        }
        $route = $this->getCurrentRoute();
        $onestep_url = wa()->getRouteUrl('shop/frontend/onestep');
        wa()->getResponse()->redirect($onestep_url);
    }

    protected function getCurrentRoute()
    {
        $storefront = (string) wa()->getRequest()->get('storefront');
        $current_domain = null;
        $current_route = null;
        $result = null;
        foreach (wa()->getRouting()->getByApp('shop') as $domain => $domain_routes) {
            $current_domain = $current_domain !== null ? $current_domain : $domain;
            foreach ($domain_routes as $route) {
                $current_route = $current_route !== null ? $current_route : $route['url'];
                $controller_url = wa()->getRouteUrl("shop/frontend/buybuttons", array(), true, $domain, $route['url']);
                if ($controller_url === $storefront) {
                    $current_domain = $domain;
                    $current_route = $route;
                }
                $root_url = rtrim($domain . '/' . $route['url'], '/*') . '/';
                $storefronts[] = array(
                    'root_url' => $root_url,
                    'controller_url' => $controller_url
                );
            }
        }
        if(!empty($current_route) && !empty($current_domain)) {
            $result['domain'] = $current_domain;
            $result['url'] = $current_route;
        }
        return $result;
    }

    public function getProduct($id)
    {
        if ($this->product === null) {
            $product = new shopProduct($id, true);
            $skus = $product->skus;
            foreach ($skus as &$sku) {
                $sku['price_html'] = shop_currency_html($sku['price'], $product['currency']);
                $sku['orig_available'] = $sku['available'];
                $sku['available'] = $this->isProductSkuAvailable($product, $sku);
            }
            unset($sku);
            $product->skus = $skus;
            $this->product = $product;
        }
        return $this->product;
    }

    private function isProductSkuAvailable($product, $sku)
    {
        return $product->status && $sku['available'] && $sku['status'] &&
            ($this->getConfig()->getGeneralSettings('ignore_stock_count') || $sku['count'] === null || $sku['count'] > 0);
    }

    protected function deteleToCart($post)
    {
        wa()->getRequest()->setParam('id', $post['id']);
        $action = new shopFrontendCartDeleteController();
        $action->execute();
        
        return true;
    }


    protected function addToCart($post)
    {
        $product_id = (int)$post['id'];
        $product = $this->getProduct($product_id);

        $available = true;

        $_POST['product_id'] = $post['id'];
        if ($product->sku_type == shopProductModel::SKU_TYPE_SELECTABLE) {
            $res = $this->featuresSelectableWorkup($product);
            $skus = $res['sku_features_selectable'];
            $features = ifset($post['features'], array());
            $key = '';
            foreach ($features as $feature_id => $feature_value_id) {
                $key .= $feature_id . ':' . $feature_value_id . ';';
            }
            if (!isset($skus[$key]) || !$skus[$key]['available'] || !$skus[$key]['status']) {
                $available = false;
            }
            $_POST['features'] = $features;
        } else if (count($product->skus) > 1) {
            $sku_id = (int) ifset($post['sku_id']);
            $skus = $product->skus;
            if (!isset($skus[$sku_id]) || !$skus[$sku_id]['available'] || !$skus[$sku_id]['status']) {
                $available = false;
            }
            $_POST['sku_id'] = $sku_id;
        }

        wa()->getRequest()->setParam('quantity', $post['quantity']);
        $action = new shopFrontendCartAddController();
        $action->execute();

        if (!empty($action->errors)) {
            // Err if product is not in the cart
            $cart_items = waUtils::getFieldValues($action->cart->items(), 'product_id', 'product_id');
            if (empty($cart_items[$product_id])) {
                $available = false;
            }
        }

        return $available;
    }
}
