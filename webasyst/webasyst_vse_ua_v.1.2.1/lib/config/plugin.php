<?php

return array(
    'name' => /*_wp*/('Add products to cart from link vse.ua'),
    'description' => /*_wp*/('Add products to cart from link vse.ua'),
    'vendor'=>'webasyst',
    'version'=>'1.2',
    'img'=>'img/logo.png',
    'shop_settings' => false,
    'frontend'    => true,
    'icons'=>array(
        16 => 'img/logo.png',
    ),
    'handlers' => array(
    ),
);
//EOF
